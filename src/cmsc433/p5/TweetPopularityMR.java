package cmsc433.p5;


import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * Map reduce which takes in a CSV file with tweets as input and output
 * key/value pairs.</br>
 * </br>
 * The key for the map reduce depends on the specified {@link TrendingParameter}
 * , <code>trendingOn</code> passed to
 * {@link #score(Job, String, String, TrendingParameter)}).
 * 
 */
public class TweetPopularityMR {

	// For your convenience...
	public static final int          TWEET_SCORE   = 1;
	public static final int          RETWEET_SCORE = 2;
	public static final int          MENTION_SCORE = 1;
	public static final int			 PAIR_SCORE = 1;

	// Is either USER, TWEET, HASHTAG, or HASHTAG_PAIR. Set for you before call to map()
	private static TrendingParameter trendingOn;

	public static class TweetMapper
	extends Mapper<LongWritable, Text, Object, IntWritable> {
		//KEYIN,VALUEIN,KEYOUT,VALUEOUT

		@Override
		public void map(/* Fill in type */ LongWritable key, /* Fill in type */ Text value, Context context)
				throws IOException, InterruptedException {
			// Converts the CSV line into a tweet object

			Tweet tweet = Tweet.createTweet(value.toString());

			// TODO: Your code goes here

			if (trendingOn == TrendingParameter.USER){ 
				//(# tweets by user) + 2*(# times retweeted) + (#times mentioned)
				context.write(new Text(tweet.getUserScreenName()), new IntWritable(1)); //(# tweets by user)

				if (tweet.wasRetweetOfUser()) {
					context.write(new Text(tweet.getRetweetedUser()), new IntWritable(2)); //2*(# times retweeted)
				}

				List<String> mentionedUsers = tweet.getMentionedUsers();
				for (String mU : mentionedUsers) {
					context.write(new Text(mU), new IntWritable(1)); //(#times mentioned)
				}
			}

			else if (trendingOn == TrendingParameter.TWEET) {
				//1 + 2*(# times retweeted)
				
				context.write(new LongWritable(tweet.getId()), new IntWritable(1)); //1

				if (tweet.wasRetweetOfTweet()) {
					context.write(new LongWritable(tweet.getRetweetedTweet()), new IntWritable(2)); //2*(# times retweeted)
				}
			}

			else if (trendingOn == TrendingParameter.HASHTAG) {
				//(# times hashtag is used)
				List<String> hashTags = tweet.getHashtags();
				for (String hT : hashTags) {
					context.write(new Text(hT), new IntWritable(1)); //(# times hashtag is used)
				}
			}

			else if (trendingOn == TrendingParameter.HASHTAG_PAIR) {
				//(# tweets the given pair of hashtags occurs in)
				List<String> hashTags = tweet.getHashtags();

				if (hashTags.size() < 2) return;
				String hT1, hT2, hTPair;

				for (int i = 0; i < hashTags.size(); i++) {
					for (int j = i + 1; j < hashTags.size(); j++) {
						hT1 = hashTags.get(i);
						hT2 = hashTags.get(j);

						if (hT1.compareTo(hT2) > 0) { //hT2 comes first alphabetically
							hTPair = "(" + hT2 + "," + hT1 + ")";
						}
						else { //hT1 comes first alphabetically (so compareTo returns negative val or 0 - natural ordering)
							hTPair = "(" + hT1 + "," + hT2 + ")";							
						}

						context.write(new Text(hTPair), new IntWritable(1)); 
						//(# tweets the given pair of hashtags occurs in)
					}
				}
			}
		}
	}

	public static class PopularityReducer 
	extends Reducer<Object, IntWritable, Object, IntWritable> {

		@Override
		public void reduce(Object key, Iterable<IntWritable> values, Context context)
				throws IOException, InterruptedException {

			IntWritable scoreSum;
			int tempSum = 0;
			Iterator<IntWritable> it = values.iterator();
			while (it.hasNext()) {
				tempSum += it.next().get();
			}
			
			scoreSum = new IntWritable(tempSum);
			if (trendingOn == TrendingParameter.TWEET) {
//				System.out.println("TWEET IN REDUCER");
				context.write((LongWritable)key, scoreSum);
			}
			else {
				context.write((Text)key, scoreSum);
			}

		}
	}

	/**
	 * Method which performs a map reduce on a specified input CSV file and
	 * outputs the scored tweets, users, or hashtags.</br>
	 * </br>
	 * 
	 * @param job
	 * @param input
	 *          The CSV file containing tweets
	 * @param output
	 *          The output file with the scores
	 * @param trendingOn
	 *          The parameter on which to score
	 * @return true if the map reduce was successful, false otherwise.
	 * @throws Exception
	 */
	public static boolean score(Job job, String input, String output,
			TrendingParameter trendingOn) throws Exception {

		TweetPopularityMR.trendingOn = trendingOn;
		
		job.setMapOutputValueClass(IntWritable.class);

		if (trendingOn == TrendingParameter.TWEET) {
			job.setMapOutputKeyClass(LongWritable.class);
		}
		else {
			job.setMapOutputKeyClass(Text.class); 
		}
		job.setJarByClass(TweetPopularityMR.class);


		// TODO: Set up map-reduce...

		job.setMapperClass(TweetMapper.class);
		job.setReducerClass(PopularityReducer.class);

		// End

		FileInputFormat.addInputPath(job, new Path(input));
		FileOutputFormat.setOutputPath(job, new Path(output));

		return job.waitForCompletion(true);
	}

}
